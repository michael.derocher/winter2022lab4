public class VideoGame {

	private String name;
	private String genre;
	private double price;
	
	public VideoGame(String name, String genre, double price) {
		this.name = name;
		this.genre = genre;		
		this.price = price;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double newPrice){
		this.price = newPrice;
	}
	
	public String getGenre(){
		return this.genre;
	}
	
	public void setGenre(String newGenre){
		this.genre = newGenre;
	}
	
	public String getName(){
		return this.name;
	}
	
	

	public void gameDescription(String name, String genre, double price) {
		
		System.out.println(name + " is a " + genre + " that cost " + price + "$.");
		
	}

}