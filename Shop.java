public class Shop {
	public static void main(String[] args) {
	
		java.util.Scanner reader = new java.util.Scanner(System.in);
	
		VideoGame[] games = new VideoGame[4];
		
		String name;
		String genre;
		double price;
	
		for (int count = 0; count < 4; count++){
			System.out.println("Game " + (count+1) + ":");
			
			System.out.println("What is the game's title?");
			name = reader.nextLine();
			System.out.println();
			
			System.out.println("What is the game's genre?");
			genre = reader.nextLine();
			System.out.println();
			
			System.out.println("What is the game's price?");
			price = reader.nextDouble();
			System.out.println();
			
			reader.nextLine();

			games[count] = new VideoGame(name, genre, price);
		}
		
		System.out.println(games[3].getName() + "  " + games[3].getGenre() + "  " + games[3].getPrice());
		
		games[3].gameDescription(games[3].getName(), games[3].getGenre(), games[3].getPrice());
		
			System.out.println("What is the game's title?");
			name = reader.nextLine();
			System.out.println("What is the game's genre?");
			genre = reader.nextLine();
			System.out.println("What is the game's price?");
			price = reader.nextDouble();
			
			games[3] = new VideoGame(name, genre, price);
			System.out.println(games[3].getName() + "  " + games[3].getGenre() + "  " + games[3].getPrice());
	}
}